TEMPLATE = lib

CONFIG += dll

CONFIG += debug_and_release

CONFIG += core
CONFIG += qt

HEADERS += ut.h
SOURCES += ut.cpp

HEADERS += debugger/*.h
SOURCES += debugger/*.cpp

QMAKE_CC = clang
QMAKE_CXX = clang++

release {
	message("Building Release")
	TARGET += ut_debug
}

debug {
	message("Building Debug")
	DEFINES += DEBUG_ADD_ALL
	DEFINES += DEBUG_TO_ALL
	DEFINES += DEBUG_ENABLE_ALL

	TARGET += ut_debug
}
